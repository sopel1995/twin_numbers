from mpi4py import MPI
import sys
import math
import time

table = []

def isPrime(n):
    if (n < 2):
        return False
    sqrt = math.sqrt(n)
    i = 2
    while i <= sqrt:
        if (n % i == 0):
            i += 1
            return False
        i += 1

    return n


def calculationOfPrimeNumbersWithCondition(min, max, lastPrime):
    prime_number_tab = []
    end_prime = lastPrime
    for x in range(min, max, 1):
        if isPrime(x):
            if end_prime == x-2:
                prime_number_tab.append(((x-2), x))
            end_prime = x

    return prime_number_tab


def calculationOfPrimeNumbers(min, max):
    prime_number_tab = []
    for x in range(min, max, 1):
        if isPrime(x):
            prime_number_tab.append(x)

    return prime_number_tab

def searchInRange(min,max):
    back_number = min
    if(min>2):
        while isPrime(back_number)!= back_number:
            back_number -= 1
    else:
        back_number = 2
    return calculationOfPrimeNumbersWithCondition(min, max, back_number)

def main():
    time_start=time.time()
    comm = MPI.COMM_WORLD
    name = MPI.Get_processor_name()
    result = searchInRange(comm.rank*10, comm.rank*10+10)
    print('Search in range ' + str(comm.rank*10) + ':' + str(comm.rank*10+10) + ' = ' + str(result) + ' in machine' + str(name))
    comm.Barrier()
    time_end = time.time()
    if comm.rank == 0:
        print(f"Time process: {time_end - time_start}")

if __name__ == '__main__':
    main()
